#!/bin/bash

# This script is meant to simplify porting and updating random configuration files
# between my many computer setups.


function main() {
    if [[ "$#" != 1 ]]; then
        echo "Wrong number of arguments given. Exiting."
        usage
        return
    fi

    arg="$1"
    
    case $arg in
        install )
            echo "Unfinished"
            ;;
        uninstall )
            echo "Unfinished"
            ;;
        upload )
            upload
            ;;
        download ) 
            download
            ;;
    esac
}

declare -A localnames
localnames[0]=tmuxconf
localnames[1]=nanorc
localnames[2]=bashrc
localnames[3]=i3conf
localnames[4]=i3stat
localnames[5]=polybarconf
localnames[6]=polybarlaunch
localnames[7]=compton

declare -A locations
locations[0]="/home/$USER/.tmux.conf"
locations[1]="/home/$USER/.nanorc"
locations[2]="/home/$USER/.bashrc"
locations[3]="/home/$USER/.config/i3/config"
locations[4]="/etc/i3status.conf"
locations[5]="/home/$USER/.config/polybar/config"
locations[6]="/home/$USER/.config/i3/polybar.sh"
locations[7]="/home/$USER/.config/comptonconf"

length_names="${#localnames[@]}"
length_locs="${#locations[@]}"

if [[ "$length_names" != "$length_locs" ]]; then
    echo "Number of names and locations doesn't match. Exiting."
    return
fi

iterator=$((length_locs - 1))

function backup() {
    # Makes a backup of the files that are about to overwritten. This should include
    # some sort of date and whatnot.
    echo "Unfinished"
}


function install() {
    # Moves the included files to their new homes.
    echo "Unfinished"
}


function uninstall() {
    # Deletes the files placed by install. There should be a backup folder created on
    # install; those files should be restored when this command runs.
    echo "Unfinished"
}


function upload() {
    # Copies files from their functional locations to the jawconf local folder so they
    # can be uploaded with git. This is performed before a git push.
    for i in $(seq 0 "$iterator"); do
        echo "Copying ${localnames[$i]} from ${locations[$i]}"
        cp "${locations[$i]}" "./local/${localnames[$i]}" || exit
    done
}


function download() {
    # Copies files from the local jawconf folder to their functional locations. This is
    # performed after a git pull.
    for i in $(seq 0 "$iterator"); do
        echo "Copying ${locations[$i]} from ${localnames[$i]}"
        cp "./local/${localnames[$i]}" "${locations[$i]}" || exit
    done
}


function usage() {
    echo "Usage: jawconf <option>"
    echo "Options: install, uninstall, upload, download."
}


main "$@"
